<?php 

namespace Cetria\Socialite;

use Cetria\Socialite\Two\BankIDProvider;
use Cetria\Socialite\Two\SeznamProvider;
use Laravel\Socialite\SocialiteManager;

class CetriaSocialiteManager extends SocialiteManager
{
    //Seznam - OAuth2 
    protected function createSeznamDriver()
    {
        $config = $this->config->get('services.seznam');

        return $this->buildProvider(
            SeznamProvider::class, $config
        );
    }

    //BankID - OpenID Connect
    protected function createBankIdDriver()
    {
        $config = $this->config->get('services.bankID');

        return $this->buildProvider(
            BankIDProvider::class, $config
        );
    }
}