<?php

namespace Cetria\Socialite\Two;

use Laravel\Socialite\Two\User;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;
use Laravel\Socialite\Two\AbstractProvider;

class SeznamProvider extends AbstractProvider
{

    /**
    * @var string[]
    */
    protected $scopes = [
        'identity',
        'avatar',
    ];
 
    /**
     * @return string
     */
    public function getSeznamUrl()
    {
        return 'https://login.szn.cz/api/v1/oauth';
    }

    /**
     * @param string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->getSeznamUrl() . '/auth', $state);
    }

    /**
     * @return string
     */
    protected function getTokenUrl()
    {
        return $this->getSeznamUrl() . '/token';
    }

    /**
     * @param string $token
     * @throws GuzzleException
     * @return array|mixed
     */
    protected function getUserByToken($token)
    {
        $this->lastToken = $token;

        $response = $this->getHttpClient()->get('https://login.szn.cz/api/v1/user', [
            RequestOptions::HEADERS => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        
        return json_decode($response->getBody(), true);
    }

    /**
     * @return User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'email' => $user['email'],
            'id' => $user['oauth_user_id'],
            'nickname' => $user['username'],
            'firstname' => $user['firstname'],
            'lastname' => $user['lastname'],
            'name' => $user['firstname'] . ' ' . $user['lastname'],
            'avatar' => $user['avatar_url'] ?? null,
        ]);
    }
}
