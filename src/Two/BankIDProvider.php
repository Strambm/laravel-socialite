<?php

namespace Cetria\Socialite\Two;

use Laravel\Socialite\Two\User;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;
use Laravel\Socialite\Two\AbstractProvider;

class BankIDProvider extends AbstractProvider
{

    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ' ';

    /**
    * @var string[]
    */
    protected $scopes = [
        'openid',
        'profile.birthnumber',
        'profile.phonenumber',
        'profile.name',
        'profile.zoneinfo',
        'profile.updatedat',
        'profile.idcards',
        'profile.email',
        'profile.birthdate',
    ];
 
    /**
     * @return string
     */
    public function getBankIDUrl()
    {
        return 'https://oidc.bankid.cz';
    }

    /**
     * @param string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->getBankIDUrl() . '/auth', $state);
    }

    /**
     * @return string
     */
    protected function getTokenUrl()
    {
        return $this->getBankIDUrl() . '/token';
    }

    /**
     * @param string $token
     * @throws GuzzleException
     * @return array|mixed
     */
    protected function getUserByToken($token)
    {
        $this->lastToken = $token;

        $response = $this->getHttpClient()->get($this->getBankIDUrl() . '/profile', [
            RequestOptions::HEADERS => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @return User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'email' => $user['email'],
            'id' => $user['sub'],
            'firstname' => $user['given_name'],
            'lastname' => $user['family_name'],
            'name' => $user['given_name'] . ' ' . $user['family_name'],
        ]);
    }
}
