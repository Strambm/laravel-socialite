# README #

Pro funkčnost je potřeba:
1. v \config\app.php přidat do pole 'providers' cestu k provideru: Cetria\Socialite\SocialiteServiceProvider::class
2. v \config\services.php přidat potřebné credentials pro daný serviceProvider: (client_id, client_secret, redirect)

## Předpoklady
- Composer - lze stáhnout z [getcomposer.org](http://getcomposer.org).

## Nainstalujte přes Composer
```
composer require cetria/laravel-socialite